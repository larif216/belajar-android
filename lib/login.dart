import 'package:belajar_dart/database_helper.dart';
import 'package:flutter/material.dart';
import 'package:belajar_dart/calculator.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'registration.dart';

class LoginPage extends StatefulWidget {
  static String tag = "login";

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  TextEditingController emailCont = TextEditingController();
  TextEditingController passwordCont = TextEditingController();

  void _login() async {
    if (emailCont.text == "" || passwordCont.text == "") {
      setState(() {
        Fluttertoast.showToast(
            msg: "Please fill empty field",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.redAccent,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
      return;
    }
    var dbHelper = DBHelper();
    var listUser = await dbHelper.getUser(emailCont.text, passwordCont.text);
    if (listUser.length > 0) {
      setState(() {
        Fluttertoast.showToast(
            msg: "Welcome to My App!",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.lightBlue,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
      Navigator.pushReplacementNamed(context, Calculator.tag);
    } else {
      setState(() {
        Fluttertoast.showToast(
            msg: "Invalid Username or Password",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.redAccent,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset("assets/logo.png"),
      ),
    );

    final email = TextFormField(
      controller: emailCont,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      controller: passwordCont,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );


    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.lightBlueAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          _login();
        },
        child: Text("Log In",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final registerLabel = FlatButton(
      child: Text(
        'Create New Account',
        style: TextStyle(
          color: Colors.black54
        ),
      ),
      onPressed: () {
        Navigator.of(context).pushNamed(RegistrationPage.tag);
      },
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(
            left: 24.0,
            right: 24.0
          ),
          children: <Widget>[
            SizedBox(
              height: 24.0,
            ),
            logo,
            SizedBox(
              height: 48.0,
            ),
            email,
            SizedBox(
              height: 8.0,
            ),
            password,
            SizedBox(
              height: 24.0,
            ),
            loginButton,
            registerLabel,
          ],
        ),
      ),
    );
  }
}