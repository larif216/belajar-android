import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import "package:path_provider/path_provider.dart";
import 'User.dart';

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "my-calculator.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE User(id INTEGER PRIMARY KEY, email TEXT, password TEXT )");
    print("Created tables");
  }

  void addUser(User user) async {
    var dbClient = await db;
    await dbClient.transaction((txn) async {
      return await txn.rawInsert(
          'INSERT INTO User(email, password ) VALUES(' +
              '\'' +
              user.email +
              '\'' +
              ',' +
              '\'' +
              user.password +
              '\'' +
              ')');
    });
  }

  Future<List<User>> getUsers(String email) async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM User');
    List<User> users = new List();
    for (int i = 0; i < list.length; i++) {
      if (list[i]["email"] == email) {
        users.add(new User(list[i]["email"], list[i]["password"]));
      }
    }
    return users;
  }

  Future<List<User>> getUser(String email, String password) async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM User');
    List<User> users = new List();
    for (int i = 0; i < list.length; i++) {
      if (list[i]["email"] == email && list[i]["password"] == password) {
        users.add(new User(list[i]["email"], list[i]["password"]));
      }
    }
    return users;

  }
}