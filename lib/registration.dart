import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'User.dart';
import 'login.dart';
import 'database_helper.dart';

class RegistrationPage extends StatefulWidget {

  static String tag = "registration";

  @override
  RegistrationPageState createState() => RegistrationPageState();
}

class RegistrationPageState extends State<RegistrationPage> {
  TextEditingController emailCont = TextEditingController();
  TextEditingController passwordCont = TextEditingController();

  void _registration() async {
    if (emailCont.text == "" || passwordCont.text == "") {
      setState(() {
        Fluttertoast.showToast(
            msg: "Please fill empty field",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.redAccent,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
      return;
    }

    if (!isEmail(emailCont.text)) {
      setState(() {
        Fluttertoast.showToast(
            msg: "Email not valid",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
      return;
    }
    var user = User(emailCont.text, passwordCont.text);
    var dbHelper = DBHelper();
    var listUser = await dbHelper.getUsers(emailCont.text);
    if (listUser.length > 0) {
      setState(() {
        Fluttertoast.showToast(
            msg: "Email already registered",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
    } else if (passwordCont.text.length < 8) {
      setState(() {
        Fluttertoast.showToast(
            msg: "Password must have at least 8 character",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
    } else {
      dbHelper.addUser(user);
      setState(() {
        Fluttertoast.showToast(
            msg: "Sign Up Success",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.lightBlue,
            textColor: Colors.white,
            fontSize: 12.0
        );
      });
      Navigator.of(context).pushNamed(LoginPage.tag);
    }

  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset("assets/logo.png"),
      ),
    );

    final email = TextFormField(
      controller: emailCont,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      controller: passwordCont,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );


    final signUpButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.lightBlueAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          _registration();
        },
        child: Text("Sign Up",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(
              left: 24.0,
              right: 24.0
          ),
          children: <Widget>[
            SizedBox(
              height: 24.0,
            ),
            logo,
            SizedBox(
              height: 48.0,
            ),
            email,
            SizedBox(
              height: 8.0,
            ),
            password,
            SizedBox(
              height: 24.0,
            ),
            signUpButton,
            SizedBox(
              height: 48.0,
            )
          ],
        ),
      ),
    );
  }

  bool isEmail(String em) {

    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }
}